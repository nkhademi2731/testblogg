import { useDispatch, useSelector } from 'react-redux';
import axios from "axios"
// actioncreators
export const SET_PRODUCT = "SET_PRODUCT";
export const SELECT_PRODUCT = "SELECT_PRODUCT"
export const REMOVE_PRODUCT = "REMOVE_PRODUCT"


export const useProduct = () => {
    const dispatch = useDispatch()
    const bstate = useSelector(state => state.Blog)
    return {
        bstate,
        dispatch,
        setProduct,
        selectProduct,
        removeProduct
    }
}

export const fetchProduct = () => async () => {

    const response = await axios.get("https://fakestoreapi.com/products").catch((err) => {
        console.log(err)
    })
    console.log(response.data);
}

export const setProduct = (products) => {
    return {
        Type: SET_PRODUCT,
        payload: products
    }
}
export const selectProduct = (product) => {
    return {
        Type: SELECT_PRODUCT,
        payload: product
    }
}
export const removeProduct = (productId) => {
    return {
        Type: REMOVE_PRODUCT,
        payload: productId
    }
}
