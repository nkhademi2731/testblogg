import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
 
} from "react-router-dom";
import './App.css';
import { Provider } from 'react-redux';

import Store from './Store';
import { Header,Footer } from './Components';
import {ProductComponent,ProductDetail} from './Components/Pages'

function App() {
  return (
    <>
      <Provider store={Store}>
      <Router>
                <Header />
                <Switch>
                    <Route path="/" exact component={ProductComponent} />
                    <Route path="/product/:productid" exact component={ProductDetail} />
                </Switch>
                <Footer />
            </Router>
      </Provider>

    </>
  );
}

export default App;
