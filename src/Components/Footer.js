import React from 'react'
import { Link } from "react-router-dom";
export default function Footer() {
    return (
        <>
            <footer className="bg-gray-800  sm:mt-10 pt-10">
                <div className="max-w-6xl m-auto text-gray-800 flex flex-wrap justify-left">

                    <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">

                        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
                            Getting Started
                        </div>

                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Installation
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Release Notes
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Upgrade Guide
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Using with Preprocessors
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Optimizing for Production
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Browser Support
                        </Link>
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            IntelliSense
                        </Link>
                    </div>


                    <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">

                        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
                            Core Concepts
                        </div>

                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Utility-First
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Responsive Design
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Hover, Focus, & Other States
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Dark Mode
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Adding Base Styles
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Extracting Components
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Adding New Utilities
                        </Link >
                    </div>


                    <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">

                        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
                            Customization
                        </div>


                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Configuration
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Theme Configuration
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Breakpoints
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Customizing Colors
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Customizing Spacing
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Configuring Variants
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Plugins
                        </Link >
                    </div>


                    <div className="p-5 w-1/2 sm:w-4/12 md:w-3/12">

                        <div className="text-xs uppercase text-gray-400 font-medium mb-6">
                            Community
                        </div>


                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            GitHub
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Discord
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            Twitter
                        </Link >
                        <Link href="#" className="my-3 block text-gray-300 hover:text-gray-100 text-sm font-medium duration-700">
                            YouTube
                        </Link >
                    </div>
                </div>


                <div className="pt-2">
                    <div className="flex pb-5 px-3 m-auto pt-5 
            border-t border-gray-500 text-gray-400 text-sm 
            flex-col md:flex-row max-w-6xl">
                        <div className="mt-2">
                            © Copyright 1998-year. All Rights Reserved.
                        </div>


                        <div className="md:flex-auto md:flex-row-reverse mt-2 flex-row flex">
                            <Link href="#" className="w-6 mx-1">
                                <i className="uil uil-facebook-f"></i>
                            </Link >
                            <Link href="#" className="w-6 mx-1">
                                <i className="uil uil-twitter-alt"></i>
                            </Link >
                            <Link href="#" className="w-6 mx-1">
                                <i className="uil uil-youtube"></i>
                            </Link >
                            <Link href="#" className="w-6 mx-1">
                                <i className="uil uil-linkedin"></i>
                            </Link >
                            <Link href="#" className="w-6 mx-1">
                                <i className="uil uil-instagram"></i>
                            </Link >
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}
