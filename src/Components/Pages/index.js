export {default as ProductComponent} from "./ProductComponent"
export {default as ProductDetail} from "./ProductDetail"
export {default as ProductList} from "./ProductList"
