import React from 'react'
import { Link } from "react-router-dom";

export default function Header() {
    return (
        <>
            <nav className="bg-indigo-400 px-4 p-4">
                <div className="flex items-center justify-between">

                    <div className="flex items-center">
                        <div>
                            <img src="https://img.icons8.com/doodle/48/000000/girl.png" />
                        </div>

                        <div className="pl-5">
                            <Link  href="">home</Link >
                            <Link  href="">about</Link >
                            <Link  href="">service</Link >
                        </div>
                    </div>
                    <div>
                        <Link className="mx-3" href="">login</Link >
                        <Link className="mx-3" href="">Sign up</Link >
                    </div>

                </div>

            </nav >

        </>
    )
}
